# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-09-21 10:45+0200\n"
"PO-Revision-Date: 2020-08-14 10:59+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: Portuguese <http://translate.tails.boum.org/projects/tails/"
"welcome_screen/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!meta title=\"Welcome Screen\"]]\n"
msgstr "[[!meta title=\"Opções de Inicialização\"]]\n"

#. type: Plain text
msgid ""
"The Welcome Screen appears after the Boot Loader, but before the GNOME "
"Desktop."
msgstr ""

#. type: Plain text
#, fuzzy
msgid ""
"You can use the Welcome Screen to specify startup options that alter some of "
"the basic functioning of Tails."
msgstr ""
"Ao iniciar o Tails, você pode especificar opções de inicialização para "
"alterar partes do seu funcionamento básico. As duas formas de especificar "
"opções de inicialização são as seguintes:"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!img welcome-screen.png link=no alt=\"Welcome to Tails!\"]]\n"
msgstr ""
"[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Bem vindo/a ao Tails. Mais\n"
"opções? Botão Sim e Não, e botão de Login.\"]]\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"To start Tails without options, click on the\n"
"<span class=\"button\">Start Tails</span> button.\n"
msgstr ""
"**Para iniciar o Tails sem opções**, clique no botão de \n"
"<span class=\"button\">Login</span>, ou simplesmente\n"
"pressione <span class=\"keycap\">Enter</span>.\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To store the settings of the Welcome Screen across different Tails sessions,\n"
"turn on the [[Welcome Screen|doc/first_steps/persistence/configure#welcome_screen]]\n"
"feature of the Persistent Storage.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>With this feature turned on, unlock your Persistent Storage to\n"
"restore your settings in the Welcome Screen.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"accessibility\"></a>\n"
msgstr "<a id=\"accessibility\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Assistive technologies"
msgstr ""

#. type: Plain text
msgid ""
"You can activate assistive technologies, like a screen reader or large text, "
"from the universal access menu (the"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img lib/preferences-desktop-accessibility.png alt=\"Accessibility Menu\" class=\"symbolic\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
msgid "icon which looks like a person) in the top bar."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"locale\"></a>\n"
msgstr "<a id=\"locale\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Language & region"
msgstr ""

#. type: Plain text
#, fuzzy
msgid ""
"You can configure Tails depending on your language and location from the "
"Welcome Screen."
msgstr ""
"Aqui está uma lista de aplicações que você pode configurar usando o <span "
"class=\"application\">Tails\n"
"Greeter</span>:\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!img locale.png link=\"no\" alt=\"\"]]\n"
msgstr ""
"[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Bem vindo/a ao Tails. Mais\n"
"opções? Botão Sim e Não, e botão de Login.\"]]\n"

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Language</span> option allows you to change the "
"main language of the interface."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  Text that is not translated yet will appear in English. You can [[help\n"
"  to translate more text|contribute/how/translate]].\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Keyboard Layout</span> option allows you to "
"change the layout of the keyboard. For example to switch to an *AZERTY* "
"keyboard which is common in France."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You will still be able to switch between different keyboard layouts from the\n"
"  desktop after starting Tails.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img introduction_to_gnome_and_the_tails_desktop/keyboard.png link=\"no\" alt=\"Keyboard Menu\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Formats</span> option allows you to change the "
"date and time format, first day of the week, measurement units, and default "
"paper size according to the standards in use in a country."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  For example, the USA and the United Kingdom, two English-speaking countries,\n"
"  have different standards:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  <table>\n"
"  <tr><th></th><th>USA</th><th>United Kingdom</th></tr>\n"
"  <tr><td>Date & time</td><td>3/17/2017 3:56 PM</td><td>17/03/2017 15:56</td></tr>\n"
"  <tr><td>First day of the week</td><td>Sunday</td><td>Monday</td></tr>\n"
"  <tr><td>Unit system</td><td>Imperial</td><td>Metric</td></tr>\n"
"  <tr><td>Paper size</td><td>Letter</td><td>A4</td></tr>\n"
"  </table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  With this option you can also display the calendar in a different language\n"
"  than the main language. For example, to display a US calendar, with weeks\n"
"  starting on Sunday, when the main language is Russian.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img US_calendar_in_Russian.png link=\"no\" alt=\"\"]]\n"
msgstr "  [[!img US_calendar_in_Russian.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"persistence\"></a>\n"
msgstr "<a id=\"persistence\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Persistent Storage"
msgstr ""

#. type: Plain text
msgid ""
"If a [[Persistent Storage|first_steps/persistence]] is detected on the USB "
"stick, an additional section appears in the Welcome Screen below the "
"**Language & Region** section:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img persistence.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img persistence.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
msgid ""
"To unlock the Persistent Storage, enter your passphrase and click **Unlock**."
msgstr ""

#. type: Plain text
msgid ""
"To create a Persistent Storage, see our instructions on [[creating a "
"Persistent Storage|first_steps/persistence/configure]]. To learn more about "
"the Persistent Storage, see our [[documentation on the Persistent Storage|"
"persistence]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"additional\"></a>\n"
msgstr "<a id=\"additional\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Additional settings"
msgstr ""

#. type: Plain text
msgid ""
"Tails is configured with care to be as safe as possible by default. But, "
"depending on your situation, you can change one of the following settings "
"from the Welcome Screen."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!img additional.png link=\"no\" alt=\"\"]]\n"
msgstr ""
"[[!img tails-greeter-welcome-to-tails.png link=no alt=\"Bem vindo/a ao Tails. Mais\n"
"opções? Botão Sim e Não, e botão de Login.\"]]\n"

#. type: Title -
#, no-wrap
msgid "Administration Password"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Set an <span class=\"guilabel\">Administration Password</span> to be\n"
"able to perform administrative tasks like installing additional\n"
"software or accessing the internal hard disks of the computer.\n"
msgstr ""

#. type: Plain text
#, fuzzy
msgid ""
"See our documentation about [[the administration password and its security "
"implications|administration_password]]."
msgstr "[[Senha para administração|administration_password]]"

#. type: Title -
#, no-wrap
msgid "MAC Address Spoofing"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Disable <span class=\"guilabel\">MAC Address Spoofing</span> to prevent\n"
"connectivity problems with your network interfaces.\n"
msgstr ""

#. type: Plain text
#, fuzzy
msgid "See our documentation about [[MAC address spoofing|mac_spoofing]]."
msgstr "[[Mudança de endereço MAC|mac_spoofing]]"

#. type: Title -
#, no-wrap
msgid "Network Configuration"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "Change the <span class=\"guilabel\">Network Configuration</span> to either:\n"
msgstr ""

#. type: Plain text
msgid "- Connect directly to the Tor network (default)."
msgstr ""

#. type: Plain text
msgid "- Configure a Tor bridge or local proxy:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"If you want to use Tor bridges because your Internet connection is censored "
"or you want to hide the fact that you are using Tor."
msgstr ""

#. type: Bullet: '  - '
msgid "If you need to use a local proxy to access the Internet."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  After starting Tails and connecting to a network, an assistant will\n"
"  guide you through the configuration of Tor.\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "  See our documentation about [[Tor bridges|bridge_mode]].\n"
msgstr "[[Mudança de endereço MAC|mac_spoofing]]"

#. type: Bullet: '- '
msgid ""
"Disable all networking if you want to work completely offline with "
"additional security."
msgstr ""

#. type: Title -
#, no-wrap
msgid "Unsafe Browser"
msgstr ""

#. type: Plain text
msgid ""
"Enable the ** *Unsafe Browser* ** to log in to a captive portal before "
"starting Tor."
msgstr ""

#. type: Plain text
msgid ""
"See our documentation about [[the *Unsafe Browser* and its security "
"implications|anonymous_internet/unsafe_browser]]."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Keyboard shortcuts"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><td><span class=\"keycap\">Alt+L</span></td><td><span class=\"guilabel\">Language</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+K</span></td><td><span class=\"guilabel\">Keyboard Layout</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+F</span></td><td><span class=\"guilabel\">Formats</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+P</span></td><td><span class=\"guilabel\">Persistent Storage</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+A</span></td><td><span class=\"guilabel\">Additional Settings</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+A</span></td><td><span class=\"guilabel\">Administration Password</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+M</span></td><td><span class=\"guilabel\">MAC Address Spoofing</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+N</span></td><td><span class=\"guilabel\">Network Configuration</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+S</td><td><span class=\"guilabel\">Start Tails</td></tr>\n"
"</table>\n"
msgstr ""

#~ msgid ""
#~ "<a id=\"boot_loader_menu\"></a>\n"
#~ "<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"
#~ msgstr ""
#~ "<a id=\"boot_loader_menu\"></a>\n"
#~ "<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"

#, fuzzy
#~ msgid "Using the <span class=\"application\">Boot Loader Menu</span>\n"
#~ msgstr "Usando o <span class=\"application\">menu de boot</span>\n"

#, fuzzy
#~ msgid ""
#~ "The <span class=\"application\">Boot Loader Menu</span> is the first "
#~ "screen to appear\n"
#~ "when Tails starts.\n"
#~ msgstr ""
#~ "O <span class=\"application\">menu de boot</span> é a primeira tela a "
#~ "aparecer\n"
#~ "quando Tails é iniciado.\n"

#, fuzzy
#~ msgid ""
#~ "<p>The <span class=\"guilabel\">Troubleshooting Mode</span> disables some "
#~ "features of the\n"
#~ "Linux kernel and might work better on some computers. You can try this "
#~ "option if you\n"
#~ "think you are experiencing errors related to hardware compatibility "
#~ "while\n"
#~ "starting Tails.</p>\n"
#~ msgstr ""
#~ "O modo <span class=\"guilabel\">failsafe</span> desabilita algumas "
#~ "funcionalidades\n"
#~ "do kernel e pode funcionar melhor em alguns computadores. Você pode "
#~ "tentar esta\n"
#~ "opção se você acha que está experienciando erros relacionados a "
#~ "compatibilidade de\n"
#~ "hardware ao iniciar o Tails.\n"

#, fuzzy
#~ msgid ""
#~ "To add a boot option, press <span class=\"keycap\">Tab</span> when the "
#~ "<span class=\"application\">Boot Loader Menu</span> appears. A list of "
#~ "boot options appears at the bottom of the screen."
#~ msgstr ""
#~ "Para adicionar uma opção de boot, pressione <span class=\"keycap\">Tab</"
#~ "span> quando o <span class=\"application\">menu de boot</span> aparecer. "
#~ "Uma lista de opções de boot aparecerão na parte de baixo da tela."

#, fuzzy
#~ msgid ""
#~ "[[!img boot-menu-with-options.png link=no alt=\"Black screen with Tails\n"
#~ "artwork. Boot Loader Menu with two options 'Tails' and 'Tails "
#~ "(Troubleshooting Mode)'.\n"
#~ "At the bottom, a list of options ending with 'vsyscall=none quiet_'\"]]\n"
#~ msgstr ""
#~ "[[!img boot-menu-with-options.png link=no alt=\"Tela preta com a arte do "
#~ "Tails.\n"
#~ "'Menu de boot' com duas opções 'Live' e 'Live (failsafe)'. Na parte de "
#~ "baixo,\n"
#~ "uma lista de opções que termina com 'noautologin quiet_'\"]]\n"

#~ msgid ""
#~ "Press <span class=\"keycap\">Space</span>, and type the boot option that "
#~ "you want to add."
#~ msgstr ""
#~ "Precione <span class=\"keycap\">Espaço</span>, e digite a opção de boot "
#~ "que você quer adicionar."

#~ msgid ""
#~ "If you want to add more than one boot option, type them one after the "
#~ "other, and separate them by a <span class=\"keycap\">Space</span>."
#~ msgstr ""
#~ "Se você quiser adicionar mais de uma opção de boot, digite-as uma após a "
#~ "outra, e separe-as por um <span class=\"keycap\">Espaço</span>."

#~ msgid "Then press <span class=\"keycap\">Enter</span> to start Tails."
#~ msgstr ""
#~ "A seguir presione <span class=\"keycap\">Enter</span> para iniciar o "
#~ "Tails."

#~ msgid ""
#~ "<a id=\"greeter\"></a>\n"
#~ "<a id=\"tails_greeter\"></a>\n"
#~ msgstr ""
#~ "<a id=\"greeter\"></a>\n"
#~ "<a id=\"tails_greeter\"></a>\n"

#~ msgid "Using <span class=\"application\">Tails Greeter</span>\n"
#~ msgstr "Usando o <span class=\"application\">Tails Greeter</span>\n"

#, fuzzy
#~ msgid ""
#~ "<span class=\"application\">Tails Greeter</span>\n"
#~ "appears after the <span class=\"application\">Boot Loader Menu</span>, "
#~ "but before the\n"
#~ "<span class=\"application\">GNOME Desktop</span>:\n"
#~ msgstr ""
#~ "<span class=\"application\">Tails Greeter</span> é um conjunto de "
#~ "diálogos que\n"
#~ "aparecem após o <span class=\"application\">menu de boot</span>, mas "
#~ "antes que\n"
#~ "o <span class=\"application\">GNOME Desktop</span> apareça. É assim que a "
#~ "primeira\n"
#~ "tela do <span class=\"application\">Tails Greeter</span> se parece:\n"

#~ msgid ""
#~ "**To start Tails in languages other than English**, select the one you\n"
#~ "want from the menu at the bottom of the screen. You can also adapt\n"
#~ "your country and keyboard layout. When you do that, <span class="
#~ "\"application\">Tails Greeter</span> itself\n"
#~ "switches language.\n"
#~ msgstr ""
#~ "**Para iniciar o Tails em outros idiomas que não inglês**, selecione o\n"
#~ "idioma que você quer a partir do menu na parte de baixo da tela. Você\n"
#~ "também pode adaptar seu país e leiaute de teclado. Quando você faz\n"
#~ "isso, é o <span class=\"application\">Tails Greeter</span> que faz a\n"
#~ "mudança do idioma de fato.\n"

#~ msgid ""
#~ "**To set more options**, click on the <span class=\"button\">Yes</span> "
#~ "button.\n"
#~ "Then click on the <span class=\"button\">Forward</span> button.\n"
#~ msgstr ""
#~ "**Para configurar mais opções**, clique no botão <span class=\"button"
#~ "\">Yes</span>.\n"
#~ "Em seguida clique no botão <span class=\"button\">Forward</span>.\n"

#, fuzzy
#~ msgid ""
#~ "  - [[Network configuration|network_configuration]]\n"
#~ "    - [[Tor bridge mode|bridge_mode]]\n"
#~ "    - [[Disabling all networking (offline_mode)|offline_mode]]\n"
#~ "  - [[Encrypted persistence|doc/first_steps/persistence/use]]\n"
#~ msgstr ""
#~ "  - [[Configuração de rede|network_configuration]]\n"
#~ "    - [[Modo Tor bridge|bridge_mode]]\n"

#, fuzzy
#~ msgid ""
#~ "Here is a list of options that you can add to the <span class="
#~ "\"application\">Boot\n"
#~ "Loader Menu</span>:\n"
#~ msgstr ""
#~ "Aqui está uma lista de opções que você pode adicionar ao <span class="
#~ "\"application\">menu\n"
#~ "de boot</span>:\n"

#~ msgid "Problems booting?\n"
#~ msgstr "Problemas ao iniciar?\n"

#~ msgid ""
#~ "If you have problems booting Tails, please read the [[following "
#~ "guidelines|doc/first_steps/bug_reporting/tails_does_not_start]]."
#~ msgstr ""
#~ "Se você tem problemas ao iniciar o Tails, por favor leia as [[seguintes "
#~ "diretivas|doc/first_steps/bug_reporting/tails_does_not_start]]."

#~ msgid "[[Windows camouflage|windows_camouflage]]"
#~ msgstr "[[Camuflagem Windows|windows_camouflage]]"

#~ msgid ""
#~ "<span class=\"command\">truecrypt</span>, to enable [[TrueCrypt|"
#~ "encryption_and_privacy/truecrypt]]"
#~ msgstr ""
#~ "<span class=\"command\">truecrypt</span>, para habilitar [[TrueCrypt|"
#~ "encryption_and_privacy/truecrypt]]"

#~ msgid "[[!toc levels=1]]\n"
#~ msgstr "[[!toc levels=1]]\n"

#, fuzzy
#~ msgid ""
#~ "  [[See our documentation about the administration password.|"
#~ "administration_password]]\n"
#~ msgstr "[[Senha para administração|administration_password]]"

#, fuzzy
#~ msgid ""
#~ "  [[See our documentation about MAC address spoofing.|mac_spoofing]]\n"
#~ msgstr "[[Mudança de endereço MAC|mac_spoofing]]"

#~ msgid "<div class=\"tip\">\n"
#~ msgstr "<div class=\"tip\">\n"
